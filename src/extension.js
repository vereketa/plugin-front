"use strict";
const {getRefTelegramBot, addKeyWords} = require('./serviceGmail.js');
const config = require('./config');

function initialize() {
  chrome.identity.getAuthToken({'interactive': true}, function (token) {
    const divEmail = document.getElementById('labelEmail');
    const bot = document.getElementById('bot');
    const labelUsername = document.getElementById('labelUserName');

    chrome.identity.getProfileUserInfo(function (userInfo) {
      const {email} = userInfo;
      divEmail.innerText = `Your email: ${email}`;

      getRefTelegramBot(email)
        .then(data => {
          const {email, subscribe, username, keyWords} = data;
          if (subscribe) {
            if(username.length > 0)
              labelUsername.innerText = `Your telegram: ${username}`;
            handleListWords(keyWords);
            handleButtonSend(email);
            return;
          }

          bot.setAttribute('href', `https://t.me/${config.bot_name}?start=gmail`)
          bot.innerText = 'Telegram bot';
        })
        .catch(e => {
          console.log(e);
          divEmail.innerText = `Invalid server`;
        });

      console.log('Token is save: ', token);
      handleShowIdExtension();
    });
  });


}

function handleShowIdExtension() {
  const divId = document.getElementById('devId');
  const strId = `Dev id: ${chrome.runtime.id}`;
  divId.innerText = strId;
}

function handleButtonSend(email) {
  const btn = document.getElementById('btn');
  const input = document.getElementById('search');

  btn.removeAttribute('disabled');
  input.removeAttribute('disabled');

  btn.addEventListener('click', e => {
    const {value} = input;
    addKeyWords(value, email).then(words => handleListWords(words));
  });
}

function handleListWords(words) {
  console.log(words.length + ' ' + words);
  const list = document.getElementById('words');
  words.forEach(word => {
    const li = document.createElement('li');
    li.innerText = word.text;
    list.append(li);
  });
}
initialize();

// 836716566:AAE-WAwnh1TpcNMmkiokPmxBQovF0P6rQPU
// ya29.Il-IB2d4CFxfoy6eDcw3L1RM8jhsxKSTWCm3WlIaN9z6iB5PmqMPf5pJgzq6DMxsNS0HSDFbmewpBOstDc-MnA4XHaTCN57YVMEjs126D27PRdZtDUm-vnEF5E4xdjcIQg
// ya29.Il-IB_KzN91EulgKaraODMowyxiAxobxpQ5GvPFgja8xe7LpT0X_i-J70HWCTc08PT3ZYixWgB5xIX_Rc6olw7da4CD9OJiPKGfy1RRA5WSR4PKyNm6tLnpjuNj_UBxsBw
