const axios = require('axios');
const config = require('./config.js');

axios.defaults.baseURL = config.request;

const getRefTelegramBot = (email) => {

  return axios.get(`/gmail/me?email=${email}`)
    .then(res => {
      return res.data;
    })
};
const addKeyWords = (search, email) => {
  return axios.post(`/gmail/keys`, {
    search,
    email,
  }, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(data => {
      return data.data;
    })
};

module.exports = { getRefTelegramBot, addKeyWords };
